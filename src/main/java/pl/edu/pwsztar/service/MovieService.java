package pl.edu.pwsztar.service;

import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.MovieCounterDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.exception.EmptyInputException;

import java.util.List;

public interface MovieService {

    List<MovieDto> findAll();

    List<MovieDto> findAllByYearDesc();

    List<MovieDto> findAllByTitle(String title);

    List<MovieDto> findAllByYear(Integer year);

    void createMovie(CreateMovieDto createMovieDto);

    void deleteMovie(Long movieId);

    MovieCounterDto countMovies();
}
